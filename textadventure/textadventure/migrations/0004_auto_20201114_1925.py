# Generated by Django 2.2.5 on 2020-11-14 18:25

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('textadventure', '0003_auto_20201114_1925'),
    ]

    operations = [
        migrations.AlterField(
            model_name='choice',
            name='station',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='choice1_set', to='textadventure.Station'),
        ),
    ]
