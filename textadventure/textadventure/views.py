from django.shortcuts import render, get_object_or_404, redirect, reverse
import random


# Create your views here.

from . import models

print("Los geht es hier: http://localhost:8000/textadventure/start")

def station(request, name="start"):
    '''
    Hier wird die passende Station geholt und ausgegeben.
    Wird kein Name übergeben, dann wird die Station 'start' geholt.
    '''
    station = get_object_or_404(models.Station, name=name) 
    savegame = models.Savegame.objects.all()
    final_random_choice= ""
    
    # Probability-Check:
    if station.station_probability == True:
        # random_number = random.uniform(0, 1)
        choice_list = []
        probability_list = []
        for choice in station.choice_set.all():
            choice_list.append(choice.next_station.name)
            for prob in choice.probability_set.all():
                probability_list.append(prob.probability)
        final_random_choice = random.choices(choice_list, probability_list, k=1)[0]

    # Inventar
    inventory = models.InventoryItem.objects.all()

    
    context = {"station": station, "savegame": savegame, "final_random_choice": final_random_choice, "inventory": inventory}
    return render(request, "textadventure/base.html", context)




def savegame(request, station_name):
    if "save_delete" in request.POST:
        save_delete = get_object_or_404(models.Savegame, name=station_name)
        save_delete.delete()
    else:
    #     if not models.Savegame.objects.filter(name=request.POST['savegame_name']):
    #         models.Savegame.objects.create(name=request.POST["savegame_name"], save_station=station_name)
    #     else:
    #         return redirect("http://127.0.0.1:8000/textadventure/"+ station_name)
        models.Savegame.objects.create(name=request.POST["savegame_name"], save_station=station_name)
    return redirect("station-default")


def take_item(request, item_to_take):
    # Items aufnehmen
    if "take_item" in request.POST:
        take_item = get_object_or_404(models.InventoryItem, name=item_to_take)
        take_item.taken = True
        take_item.save()

    station = take_item.station.name
    
    return redirect("station", station)
