from django.db import models

# Create your models here.


class Station(models.Model):
    '''
    Eine Station im Textadventure. Der Name ist eine sprechende ID,
    muss also eindeutig (unique) sein.
    '''
    name = models.CharField(max_length=50, unique=True)
    station_probability = models.BooleanField(default=False)
    title = models.CharField(max_length=200)
    text = models.TextField()
    image = models.TextField(null=True, default="textadventure/256x256.png")
    music = models.TextField(null=True, default="textadventure/adventure.mp3")

    def __str__(self):
        return self.name


class Choice(models.Model):
    '''
    Eine Auswahloptin. Neben dem Text zur Beschreibung enthält Sie auch den 
    Verweis auf die nächste Station.
    '''
    station = models.ForeignKey(to=Station, on_delete=models.CASCADE, related_name="choice_set")
    text = models.CharField(max_length=200)
    next_station = models.ForeignKey(to=Station, on_delete=models.CASCADE, related_name="incoming_set")
    sound = models.TextField(null=True, default="textadventure/menu-choice.mp3")

    def __str__(self):
        return f"{self.text}: {self.next_station.name}" 


class Probability(models.Model):
    choice = models.ForeignKey(to=Choice, on_delete=models.CASCADE)
    probability = models.FloatField()
    def __str__(self):
        return f"{self.choice}: {self.probability}" 


class Savegame(models.Model):
    name = models.CharField(max_length=50)
    save_station = models.CharField(max_length=50)
    def __str__(self):
        return f"{self.name}: {self.save_station}"


class InventoryItem(models.Model):
    name = models.CharField(max_length=100)
    text = models.TextField()
    station = models.ForeignKey(to=Station, on_delete=models.CASCADE)
    taken = models.BooleanField(default=False)
